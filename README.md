
# Local Setup for the Service

### Start the redis in docker
```bash
docker run -d \
	--name redis_cache \
	-v cache:/data \
    -p 6379:6379 \
	redis:6.2-alpine
```

### Run the application
```bash
cargo watch -x "run -- local" --clear
```


# Local Setup for the Service in Docker 

### NOTE
* If new queries were added then this would require to prepare the .sqlx json files for compilation by running
```bash
cargo sqlx prepare
```

### Starting the services, always start in detach mode

```bash
docker compose up --no-deps --build -d
```

** If changes to the db configuration take place it is best to rebuild the image
```bash
docker compose build --no-cache
```


# Bulding and pushing the image

```bash
aws ecr get-login-password --region ca-central-1 --profile trnip | docker login --username AWS --password-stdin 676180064909.dkr.ecr.ca-central-1.amazonaws.com
```

```bash
docker build -t authentication_api:latest ./api -f ./api/dockerfiles/Dockerfile.prod
```

```bash
docker tag authentication_api:latest 676180064909.dkr.ecr.ca-central-1.amazonaws.com/trnip/authentication_api:latest
```

```bash
docker push 676180064909.dkr.ecr.ca-central-1.amazonaws.com/trnip/authentication_api:latest
```

pub mod models;

use models::AuthTokenModel;
use sqlx::{self, Pool, Postgres, postgres::PgPoolOptions};
use tracing::error;
use uuid::Uuid;

pub async fn establish_connection() -> Pool<Postgres> {
    let database_url = std::env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    match PgPoolOptions::new()
        .max_connections(10)
        .connect(&database_url)
        .await
    {
        Ok(pool) => {
            return pool
        }
        Err(err) => {
            error!("🔥 Failed to connect to the database: {:?}", err);
            std::process::exit(1);
        }
        
    };
}

pub async fn insert_user_tokens(db: &Pool<Postgres>, user_tokens: &AuthTokenModel) -> Result<AuthTokenModel, sqlx::Error> {
    let query_result = sqlx::query_as!(
        AuthTokenModel,
        r#"
            INSERT INTO auth_tokens (user_id, access_token, refresh_token, access_expires_at, refresh_expires_at) 
            VALUES ($1, $2, $3, $4, $5) RETURNING *
        "#,
        user_tokens.user_id,
        user_tokens.access_token,
        user_tokens.refresh_token,
        user_tokens.access_expires_at,
        user_tokens.refresh_expires_at
    )
    .fetch_one(db)
    .await;

    match query_result {
        Ok(record) => Ok(record),
        Err(err) => {
            error!("Query returned with error: {:?}", err);
            Err(err)
        }
    }
}

pub async fn update_user_tokens(db: &Pool<Postgres>, user_tokens: &AuthTokenModel) -> Result<AuthTokenModel, String> {
    let query_result = sqlx::query_as!(
        AuthTokenModel,
        r#"
            UPDATE auth_tokens SET
                access_token = $1,
                refresh_token = $2, 
                access_expires_at = $3, 
                refresh_expires_at = $4,
                refreshed_at = $5
            WHERE user_id = $6
            RETURNING *
        "#,
        user_tokens.access_token,
        user_tokens.refresh_token,
        user_tokens.access_expires_at,
        user_tokens.refresh_expires_at,
        user_tokens.refreshed_at,
        user_tokens.user_id
    )
    .fetch_one(db)
    .await;

    match query_result {
        Ok(record) => Ok(record),
        Err(sqlx::Error::RowNotFound) => {
            Err(format!("User doesn't exist"))
        },
        Err(err) => {
            error!("Update returned with error: {:?}", err);
            Err(format!("Somehting went wrong: {:?}", err))
        }
    }
}

pub async fn get_user_token_by_user_id(db: &Pool<Postgres>, user_id: &Uuid) -> Result<AuthTokenModel, String> {
    let query_result = sqlx::query_as!(
        AuthTokenModel,
        r#"
            SELECT * FROM auth_tokens 
            WHERE user_id = $1
        "#, 
        user_id
    )
    .fetch_one(db)
    .await;

    match query_result {
        Ok(record, ) => Ok(record),
        Err(sqlx::Error::RowNotFound) => {
            Err(format!("User doesn't exist"))
        },
        Err(err) => {
            error!("Update returned with error: {:?}", err);
            Err(format!("Somehting went wrong: {:?}", err))
        }
    }
}




// pub async fn get_tokens_by_user_id(db: &Pool<Postgres>, user_id: &Uuid) -> Result<AuthTokenModel, String> {
//     let query_result = sqlx::query_as!(
//         AuthTokenModel,
//         r#"
//             SELECT * FROM auth_tokens 
//             WHERE user_id = $1
//         "#,
//         user_id
//     ).fetch_one(db)
//     .await;
     
//     match query_result {
//         Ok(record) => Ok(record),
//         Err(sqlx::Error::RowNotFound) => {
//             Err(format!("User doesn't exist"))
//         },
//         Err(err) => {
//             error!("Query returned with error: {:?}", err);
//             Err(format!("Somehting went wrong: {:?}", err))
//         }
//     }
// }
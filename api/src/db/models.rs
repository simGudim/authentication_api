use serde::{Deserialize, Serialize};
use sqlx::FromRow;
use uuid::Uuid;

#[derive(Debug, FromRow, Deserialize, Serialize)]
#[allow(non_snake_case)]
pub struct AuthTokenModel {
    pub user_id: Uuid,
    pub access_token: String,
    pub refresh_token: String,
    pub access_expires_at: Option<chrono::DateTime<chrono::Utc>>,
    pub refresh_expires_at: Option<chrono::DateTime<chrono::Utc>>,
    pub created_at: Option<chrono::DateTime<chrono::Utc>>,
    pub refreshed_at: Option<chrono::DateTime<chrono::Utc>>
}


#[derive(Debug, Serialize, Deserialize)]
pub struct TokenClaims{
    pub user_id: Uuid,
    pub token_type: String,
    pub iat: i64,
    pub exp: i64
 }







mod conf;
mod cache;
mod db;
mod routes;
mod utils;

use crate::conf::Config;
use cache::RedisConnectionPool;

use actix_cors::Cors;
use actix_web::{web, App,HttpServer, http};  
use sqlx::{Pool, Postgres};
use tracing_subscriber;

fn routes(cfg: &mut web::ServiceConfig) {
    let scope = web::scope("/auth_api")
        .service(routes::create_tokens::insert_create_tokens_endpoint)
        .service(routes::refresh_tokens::refresh_tokens_endpoint)
        .service(routes::update_tokens::update_tokens_endpoint)
        .service(routes::liveness_probe::ping_liveness);
    cfg.service(scope);
}

pub struct AppState {
    db: Pool<Postgres>,
    cache: RedisConnectionPool
}

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    let config = Config::from_env().expect("Server configuration not set");
    tracing_subscriber::fmt().json().init();
    let pool = db::establish_connection().await;   
    let cache_pool = cache::establish_redis_connection().await;

    HttpServer::new(move || {
        let cors = Cors::default()
            .allowed_origin("http://localhost:8000")
            .allowed_origin("http://localhost:3000")
            .allowed_methods(vec!["GET", "POST"])
            .allowed_headers(vec![http::header::AUTHORIZATION, http::header::ACCEPT])
            .allowed_header(http::header::CONTENT_TYPE)
            .max_age(3600);
        App::new()
            .wrap(cors)
            .app_data(web::Data::new(AppState{
                db: pool.clone(),
                cache: cache_pool.clone()
            }))
            .configure(routes)
    })
    .bind(format!("{}:{}", config.host, config.port))?
    .run()
    .await?;
    Ok(())
}

use std::{env, ops::DerefMut, time::Duration};
use tracing::error;
use r2d2_redis::{r2d2, RedisConnectionManager, redis};

pub type RedisConnectionPool = r2d2::Pool<RedisConnectionManager>;

pub async fn establish_redis_connection() -> RedisConnectionPool {
    let redis_url = env::var("REDIS_URL").expect("REDIS_URL is not set");
    let manager = RedisConnectionManager::new(redis_url);
    match manager {
        Ok(client) => {
            let pool = r2d2::Pool::builder()
                .build(client);
            match pool { 
                Ok(redis_pool) => { return redis_pool },
                Err(err) => {
                    error!("🔥 Failed to connect redis connection pool: {:?}", err);
                    std::process::exit(1);
                }
            }
        },
        Err(err) => {
            error!("🔥 Failed to connect to the cache: {:?}", err);
            std::process::exit(1);
        }
    }
}


pub fn blacklist_token(refresh_token: &str, redis_pool: &RedisConnectionPool) -> Result<String, String> {
    let connection = redis_pool.get_timeout(Duration::from_secs(10));
    match connection {
        Ok(mut conn) => {
            let result = redis::cmd("SETEX")
                .arg(refresh_token)
                .arg(30 * 24 * 60 * 60)  // Expiration time in seconds (30 days)
                .arg(1)
                .query::<String>(conn.deref_mut());
            if result.is_ok() {
                return Ok(result.unwrap())
            } else{
                return Err(format!("Redis returned error upons set command: {}", result.unwrap_err()))
            }
        },
        Err(err) => {
            error!("Could not get a redis connection: {}", err);
            return Err(format!("Could not get a redis connection: {}", err))
        }
    }
}

pub fn get_blacklisted_token(refresh_token: &str, redis_pool: &RedisConnectionPool) -> Result<Option<i8>, String> {
    let connection = redis_pool.get_timeout(Duration::from_secs(10));
    match connection {
        Ok(mut conn) => {
            let result = redis::cmd("GET")
                .arg(refresh_token)
                .query::<Option<i8>>(conn.deref_mut());
            if result.is_ok() {
                return Ok(result.unwrap())
            } else{
                return Err(format!("Redis returned error upon get command: {}", result.unwrap_err()))
            }
        },
        Err(err) => {
            error!("Could not get a redis connection: {}", err);
            return Err(format!("Could not get a redis connection: {}", err))
        }
    }
}
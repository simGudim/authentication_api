use crate::db::models::TokenClaims;
use chrono::{
    Utc, 
    Duration, 
    DateTime
};
use std::env;
use jsonwebtoken::{
    encode, 
    decode, 
    Header, 
    EncodingKey, 
    DecodingKey,
    Validation, 
    Algorithm,
    TokenData
};
use uuid::Uuid;


pub fn encode_token(
    token_claims: &TokenClaims
) -> Result<String, String>  {
    let secret_key = env::var("AUTH_SECRET_KEY").expect("could not find the auth secret key");
    let access_token = encode(
        &Header::default(), 
        &token_claims, 
        &EncodingKey::from_secret(secret_key.as_ref())
    );
    match access_token {
        Ok(at) => Ok(at),
        Err(err) => Err(format!("Somehting went wrong with encoding: {:?}", err))
    }
}

pub fn decode_token(token: &String) -> Result<TokenData<TokenClaims>, String>{
    let secret_key = env::var("AUTH_SECRET_KEY").expect("could not find the auth secret key");
    let validation = Validation::new(Algorithm::HS256);
    let token_data = decode::<TokenClaims>(
        token.as_str(), 
        &DecodingKey::from_secret(secret_key.as_ref()), 
        &validation
    );

    match token_data {
        Ok(td) => Ok(td),
        Err(err) => Err(format!("Somehting went wrong with decoding: {:?}", err))
    }
}

pub fn get_token_claims(user_id: Uuid) -> (TokenClaims, TokenClaims) {
    let current_timestamp = Utc::now();
    let access_expiry = current_timestamp.clone() + Duration::minutes(30);
    let refresh_expiry = current_timestamp.clone() + Duration::days(30);

    let access_token_claims = TokenClaims {
        user_id : user_id,
        token_type: "access_token".to_owned(),
        iat: current_timestamp.timestamp(),
        exp: access_expiry.timestamp()
    };
    
    let refresh_token_claims = TokenClaims {
        user_id : user_id,
        token_type: "refresh_token".to_owned(),
        iat: current_timestamp.timestamp(),
        exp: refresh_expiry.timestamp()

    };

    return (access_token_claims, refresh_token_claims)
}

pub fn get_datetime_from_token_claims(unix_timestamp: &i64) -> Option<DateTime<Utc>> {
    Some(
        DateTime::from_timestamp(
            *unix_timestamp, 
            0
        ).expect("could not convert unix timestamp frpm claim")
    )
}
use crate::AppState;
use crate::db::models::{AuthTokenModel};
use crate::db::update_user_tokens;
use crate::utils::crypto::{
    encode_token, 
    get_token_claims, 
    get_datetime_from_token_claims
};
use actix_web::{HttpResponse, Result, post, web, http};
use serde::{Deserialize, Serialize};
use tracing::{info, error};
use uuid::Uuid;

const INDEX: &'static str = "update_tokens";
const SERVICE: &'static str = "auth_api";

#[derive(Serialize, Deserialize, Debug)]
pub struct IncomingRequest {
    pub user_id: Uuid
}


type Request = web::Json<IncomingRequest>;

/*
Exampe Usage:
```bash
curl --header "Content-Type: application/json" \
--data '{"user_id" : "e41fb885-6f09-4487-871a-b12fe71b94c5"}' \
http://localhost:8001/auth_api/update_tokens
```
*/

#[post("/update_tokens")]
pub async fn update_tokens_endpoint(pool: web::Data<AppState>, request: Request) -> Result<HttpResponse> {
    let db_pool = pool.db.clone();
    let (access_token_claims, refresh_token_claims) = get_token_claims(request.user_id);

    match (encode_token(&access_token_claims), encode_token(&refresh_token_claims)) {
        (Ok(access_token), Ok(refresh_token)) => {
            let auth_token_record = AuthTokenModel {
                user_id : request.user_id,
                access_token: access_token,
                refresh_token: refresh_token,
                access_expires_at: get_datetime_from_token_claims(&access_token_claims.exp),
                refresh_expires_at: get_datetime_from_token_claims(&refresh_token_claims.exp),
                created_at: get_datetime_from_token_claims(&access_token_claims.iat),
                refreshed_at: get_datetime_from_token_claims(&access_token_claims.iat)
            };
            match update_user_tokens(&db_pool, &auth_token_record).await {
                Ok(result) => {
                    info!(
                        target: "update_tokens_endpoint",
                        status_code = 200, 
                        status = "SUCCESS", 
                        service = SERVICE,
                        index = INDEX,
                        user_id = request.user_id.to_string()
                    );
                    return Ok(HttpResponse::Ok()
                        .status(http::StatusCode::OK)
                        .json(result)
                    )
                },
                Err(insert_error) => {
                    error!(
                        target: "update_tokens_endpoint",
                        message = insert_error.to_string(),
                        status_code = 500, 
                        status = "ERROR", 
                        service = SERVICE,
                        index = INDEX,
                        user_id = request.user_id.to_string()
                    );
                    return Ok(HttpResponse::InternalServerError()
                        .status(http::StatusCode::INTERNAL_SERVER_ERROR)
                        .body(format!("Could not update tokens in db: {}", insert_error))
                    )
                }
            }
        },
        _ => {
            error!(
                target: "update_tokens_endpoint",
                status_code = 500, 
                status = "ERROR", 
                service = SERVICE,
                index = INDEX,
                user_id = request.user_id.to_string()
            );
            return Ok(HttpResponse::InternalServerError()
                .status(http::StatusCode::INTERNAL_SERVER_ERROR)
                .body(format!("Could not encode token claims"))
            )
        }
    }
}


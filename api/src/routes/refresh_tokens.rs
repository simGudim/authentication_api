use crate::AppState;
use crate::db::models::{AuthTokenModel, TokenClaims};
use crate::db::update_user_tokens;
use crate::cache::{blacklist_token, get_blacklisted_token};
use crate::utils::crypto::{
    decode_token, 
    encode_token,
    get_token_claims,
    get_datetime_from_token_claims
};
use actix_web::{HttpResponse, Result, post, web, http};
use chrono::{Utc, Duration, DateTime};
use serde::{Deserialize, Serialize};
use tracing::{info, error};


const INDEX: &'static str = "refresh_tokens";
const SERVICE: &'static str = "auth_api";

#[derive(Serialize, Deserialize, Debug)]
pub struct IncomingRequest {
    pub refresh_token: String
}


type Request = web::Json<IncomingRequest>;

/*
Exampe Usage:
```bash
curl --header "Content-Type: application/json" \
--data '{"refresh_token" : "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoiMmM4ZGVlZjEtYTBlMy00OTZlLTlkZjYtZDE2MzVjNjQ3MzBkIiwidG9rZW5fdHlwZSI6InJlZnJlc2hfdG9rZW4iLCJleHBpcmVzX2F0IjoiMjAyNC0wNC0xMFQyMjoyODo1Mi45MjM0ODI2NDdaIn0.YX8TFImREpOuUjrdbYbk28FGfroB1Ca3DiMVw4ZWGpw"}' \
http://localhost:8001/auth_api/refresh_tokens
```
*/

#[post("/refresh_tokens")]
pub async fn refresh_tokens_endpoint(pool: web::Data<AppState>, request: Request) -> Result<HttpResponse> {
    match decode_token(&request.refresh_token) {
        Ok(decoded_token) => {
            let user_id = decoded_token.claims.user_id;
            let token_type = decoded_token.claims.token_type;
            let redis_pool = pool.cache.clone();
            let is_blacklisted = get_blacklisted_token(&request.refresh_token, &redis_pool);
            
            if !is_blacklisted.is_ok() {
                let err = &is_blacklisted.unwrap_err();
                error!(
                    target: "refresh_tokens_endpoint",
                    message = &err,
                    status_code = 500, 
                    status = "ERROR", 
                    service = SERVICE,
                    index = INDEX,
                    user_id = user_id.to_string()
                );
                return Ok(HttpResponse::InternalServerError()
                    .status(http::StatusCode::INTERNAL_SERVER_ERROR)
                    .body(format!("Could not get blacklisted token: {}", err))
                )           
            }
            let blacklisted_result = is_blacklisted.unwrap();

            if (token_type == "refresh_token") &&(!blacklisted_result.is_some()) {
                let db_pool = pool.db.clone();
                let (access_token_claims, refresh_token_claims) = get_token_claims(user_id);

                match (encode_token(&access_token_claims), encode_token(&refresh_token_claims)) {
                    (Ok(access_token), Ok(refresh_token)) => {
                        let auth_token_record = AuthTokenModel {
                            user_id : user_id.clone(),
                            access_token: access_token,
                            refresh_token: refresh_token,
                            access_expires_at: get_datetime_from_token_claims(&access_token_claims.exp),
                            refresh_expires_at: get_datetime_from_token_claims(&refresh_token_claims.exp),
                            created_at: get_datetime_from_token_claims(&access_token_claims.iat),
                            refreshed_at: get_datetime_from_token_claims(&access_token_claims.iat)
                        };
                        match update_user_tokens(&db_pool, &auth_token_record).await {
                            Ok(result) => {
                                let redis_result = blacklist_token(&request.refresh_token, &redis_pool);
                                match redis_result {
                                    Ok(_) => {
                                        info!(
                                            target: "refresh_tokens_endpoint",
                                            status_code = 200, 
                                            status = "SUCCESS", 
                                            service = SERVICE,
                                            index = INDEX,
                                            user_id = user_id.to_string()
                                        );
                                        return Ok(HttpResponse::Ok()
                                            .status(http::StatusCode::OK)
                                            .json(result)
                                        )
                                    },
                                    Err(err) => {
                                        error!(
                                            target: "refresh_tokens_endpoint",
                                            message = err,
                                            status_code = 500, 
                                            status = "ERROR", 
                                            service = SERVICE,
                                            index = INDEX,
                                            user_id = user_id.to_string()
                                        );
                                        return Ok(HttpResponse::InternalServerError()
                                            .status(http::StatusCode::INTERNAL_SERVER_ERROR)
                                            .body(format!("Could not blacklist token: {}", err))
                                        )
                                    }
                                }
                            },
                            Err(insert_error) => {
                                error!(
                                    target: "refresh_tokens_endpoint",
                                    message = insert_error.to_string(),
                                    status_code = 500, 
                                    status = "ERROR", 
                                    service = SERVICE,
                                    index = INDEX,
                                    user_id = user_id.to_string()
                                );
                                return Ok(HttpResponse::InternalServerError()
                                    .status(http::StatusCode::INTERNAL_SERVER_ERROR)
                                    .body(format!("Could not update tokens: {}", insert_error))
                                )
                            }
                        }
                    },
                    _ => {
                        error!(
                            target: "refresh_tokens_endpoint",
                            status_code = 500, 
                            status = "ERROR", 
                            service = SERVICE,
                            index = INDEX,
                            user_id = user_id.to_string()
                        );
                        return Ok(HttpResponse::InternalServerError()
                            .status(http::StatusCode::INTERNAL_SERVER_ERROR)
                            .body(format!("Could not encode token claims"))
                        )
                    }
                }
            } else {
                info!(
                    target: "refresh_tokens_endpoint",
                    message = format!(
                        " token blacklisted: {} or token type is incorrect: {}",
                        blacklisted_result.unwrap_or_default(), token_type
                    ),
                    status_code = 200, 
                    status = "SUCCESS", 
                    service = SERVICE,
                    index = INDEX,
                    user_id = user_id.to_string(),
                    token_type = token_type,
                    blacklisted_result = blacklisted_result
                );
                return Ok(HttpResponse::InternalServerError()
                    .status(http::StatusCode::UNAUTHORIZED)
                    .body(format!(
                        "token blacklisted: {} or token type is incorrect: {}",
                        blacklisted_result.unwrap_or_default(), token_type
                    ))
            )
            }
        },
        Err(decoding_error) => {
            error!(
                target: "refresh_tokens_endpoint",
                message = format!("{}", decoding_error),
                status_code = 500, 
                status = "ERROR", 
                service = SERVICE,
                index = INDEX
            );
            return Ok(HttpResponse::InternalServerError()
                .status(http::StatusCode::INTERNAL_SERVER_ERROR)
                .body(format!("Could not decode token"))
            )
        }
    }
}


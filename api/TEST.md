# create tokens
```bash
curl --header "Content-Type: application/json" \
--data '{"user_id" : "2c8deef1-a0e3-496e-9df6-d1635c64730d"}' \
http://0.0.0.0:8001/auth_api/create_tokens
```

# refresh tokens
```bash
curl --header "Content-Type: application/json" \
--data '{"refresh_token" : "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoiMTE5MGEzOWQtZWQ4My00MzU1LWEzMGMtODM3ZjhiMWQ0ZDU4IiwidG9rZW5fdHlwZSI6InJlZnJlc2hfdG9rZW4iLCJpYXQiOjE3MDc2NzA5NTYsImV4cCI6MTcwNzY3MDk1N30.0VGT-css-b0icNtQnLuUlxHnGetRZxPeGC_F7BYXM2Y"}' \
http://localhost:8001/auth_api/refresh_tokens
```